#!/usr/bin/env python3.5

from netmiko import ConnectHandler
import csv


with open('devices.csv', 'r') as file:
    lister = csv.reader(file)
    for row in lister:
        
        CiscoDevices = {
            'device_type': 'cisco_ios',
            'ip': row[2],
            'username': row[3],
            'password': row[4],
        }

        connect_device = ConnectHandler(**CiscoDevices)
        config_command = ['hostname ' + row[0], 'do write mem']
        output = connect_device.send_config_set(config_command)
        print(output)
