#!/usr/bin/env python3


from netmiko import ConnectHandler
import csv

from typing import Any, Union

with open('../devices.csv', 'r') as devices:
    reader = csv.reader(devices)
    for rows in reader:
        ciscoDevices = {
            'device_type': 'cisco_ios',
            'ip': rows[2],
            'username': rows[3],
            'password': rows[4],
        }

        with open('../baseConfigPython.txt', 'r') as commands:
            lines = commands.read().splitlines()
            print(lines)
            device_connection = ConnectHandler(**ciscoDevices)
            output = device_connection.send_config_set(lines)
            with open('output.txt', 'a') as out:
                out.write('\n' + output)
            print(output)



