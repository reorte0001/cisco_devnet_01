#!/usr/bin/env python3.5

from netmiko import ConnectHandler
import csv

with open('../devices.csv', 'r') as file:
    lister = csv.reader(file)
    for row in lister:
        
        CiscoDevices = {
            'device_type': 'cisco_ios',
            'ip': row[2],
            'username': row[3],
            'password': row[4],
        }

        connect_device = ConnectHandler(**CiscoDevices)
        output = connect_device.send_command('show ver | include uptime')
        print(output)

        connect_device = ConnectHandler(**CiscoDevices)
        output = connect_device.send_command('show ip int bri  | exclude unass')
        print(output)

        connect_device = ConnectHandler(**CiscoDevices)
        output = connect_device.send_command('show run | sec username')
        print(output)