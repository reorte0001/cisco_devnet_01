#!/usr/bin/env python3

from netmiko import ConnectHandler
import getpass

device_no = input('Ingrese el numero de dispositivo: ')
ssh_user = input('Ingrese el nombre de usuario de conexion: ')
ssh_pass = getpass.getpass('Ingrese el password de conexion: ')
enab_pass = getpass.getpass('Ingrese el passwor del modo Enable: ')

equipos = {
    'ip': '172.28.{}'.format(device_no) + '.2',
    'username': ssh_user,
    'password': ssh_pass,
    'secret': enab_pass,
    'device_type': 'cisco_ios',
}

device_connect = ConnectHandler(**equipos)
device_connect.enable()
print('Ingresando al modo Enable...')



def config_if(interface):
    output = device_connect.send_command('show run')
    if interface in output:
        command = [interface, 'shutdown']
        device_connect.send_config_set(command)
        result = device_connect.send_command('show run ' + interface)
        print(result)
    else:
        print('La interfaz ' + interface + ' no esta definida en el dispositivo')


config_if('interface GigabitEthernet0/3')
config_if('interface GigabitEthernet1/3')
config_if('interface GigabitEthernet2/3')
